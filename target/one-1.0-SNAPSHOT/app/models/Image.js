define([
    'backbone-min'
], function (Backbone) {
    "use strict";

    var Image = Backbone.Model.extend({
        urlRoot: apiURL + 'api/Images'
    });

    var Images = Backbone.Collection.extend({
        url: apiURL + 'api/Images'
    });

    var Image1Param = Backbone.Model.extend({
        initialize: function (models, options) {
            this.id = options.imageId;
        },
        urlRoot: function () {
            return apiURL + 'api/Images/' + this.id;
        }
    });

    return {
        Image: Image,
        Images: Images,
        Image1Param: Image1Param
    };
});