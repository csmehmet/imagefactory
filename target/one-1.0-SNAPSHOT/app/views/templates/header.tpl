<div class="container" id="menu">
    <div class="navbar-header">
        <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
            <span class="sr-only">Toggle navigation</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
        </button>
        <a class="navbar-brand" style="background: lightsteelblue" href="#">
            <span data-i18n="app.name"></span>
        </a>
        <a href="#/pages/image" class="navbar-brand" id="userOperations">
            <span data-i18n="header.home"></span>
        </a>
    </div>
    <div id="navbar" class="navbar-collapse collapse">
        <ul class="nav navbar-nav navbar-right">
            <li class="dropdown">
                <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true"
                   aria-expanded="false">
                    <span data-i18n="header.language.language"></span>
                    <span class="caret"></span>
                </a>
                <ul class="dropdown-menu">
                    <li>
                        <a role="button" id="langTR">
                            <span data-i18n="header.language.turkish"></span>
                        </a>
                    </li>
                    <li>
                        <a role="button" id="langEN">
                            <span data-i18n="header.language.english"></span>
                        </a>
                    </li>
                    <li>
                        <a role="button" id="langFR">
                            <span data-i18n="header.language.french"></span>
                        </a>
                    </li>
                </ul>
            </li>
            <li>
                <a role="button" id="logout">
                    <span data-i18n="header.exit"></span>
                </a>
            </li>
        </ul>
    </div><!--/.nav-collapse -->
</div>