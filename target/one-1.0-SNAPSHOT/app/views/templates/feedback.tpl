<div class="alert {{type}}">
    <div class="close-button"></div>
    <ul class="body">
        {{#each messages}}
        <li><span class="alert-header">{{../type}}</span><span data-i18n="{{key}}">{{text}}</span></li>
        {{/each}}
    </ul>
</div>