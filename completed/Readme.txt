1.  Kullanılan Teknolojiler:
    1.1 Front-End:
        Backbone, underscore, jquery, handlebars, i18n, alertify, bootstrap
    1.2 Back-End:
        Java 1.8, spring, maven 3.5.0, jpa-hibernate, cxf-jaxrs, log4j
    1.3 Database
        Mysql
    1.4 Web Srever
        Maven Jetty Plugin

2. Yapılan İşler
    - Resimleri Listeleme
    - Resim ve açıklama ekleme (tarih back-end kısmında eklenir.)
    - Resim silme
    - Resimler güne göre azalan, saate göre artan şekilde listelenir. Çözüm -> [ImageDaoImpl.java->getList()]
    - Resim eklenirken 5250000 byte üzeri resim kabul edilmez. Bu değer de property den okunur. Çözüm -> [ImageService.java->addImage()]
    - En fazla n adet resim yüklenebilir. Bu değer property den okunur. Daha fazla resim yüklenilmek istendiğinde en büyük byte ye sahip olan resim silinir. Çözüm -> [ImageService.java->addImage()]
    - Uygulama içinde loglama için log4j kütüphanesi kullanılmıştır. Log derecesi INFO şeklinde ayarlanmıştır.
    - Hata yönetim mekanizması yazılmıştır(GenericException.java) Tahmin edilen hatalar alt katmanlarda handle edilip ana katmana döndürülmüş,
      ana katmandan da front-end kısmına response olarak döndürülüp alertify.js ile popup şeklinde ekrana çıkartılmıştır.
    - Front-end kısma i18n ile Türkçe İngilizce dil desteği eklenmiştir.
    - Projenin akışı dökümanlarda belirtilmiştir.

3. Projeyi Çalıştırma
    - Proje adresi: http://localhost:8080/project/#/pages/image
    - Api adresi:   http://localhost:8080/project/api/Images
    - Db ayarları:  hibernate.cfg.xml

4. Mysql Table Create Script
    use completedsch;
    CREATE TABLE `images` (
       `ID` int(11) NOT NULL AUTO_INCREMENT,
       `IMAGE` longtext,
       `IMAGE_DESC` varchar(100) DEFAULT NULL,
       `CREATE_DATE` datetime DEFAULT NULL,
       PRIMARY KEY (`ID`),
       UNIQUE KEY `CARID_UNIQUE` (`ID`)
     ) ENGINE=InnoDB AUTO_INCREMENT=54 DEFAULT CHARSET=utf8

5. War File
    Target klasörü altındadır. Jetty-runner üzerinde koşturabilirsiniz.