package com.completed.one.dao.impl;

import com.completed.one.dao.ImageDao;
import com.completed.one.domain.Image;
import com.completed.one.utils.HibernateUtil;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.criterion.Projections;

import java.util.List;

public class ImageDaoImpl implements ImageDao {

    public Image getById(Integer imageId) {
        Session session = HibernateUtil.getSessionFactory().openSession();
        session.beginTransaction();
        Image image  = session.get(Image.class, imageId);
        session.close();

        return image;
    }

    public List<Image> getList() {
        Session session = HibernateUtil.getSessionFactory().openSession();
        session.beginTransaction();
        /*Tarihe göre azalan, saate göre artan şekilde kayıtları getir.*/
        List<Image> imageList  = session.createQuery("from Image im order by date(im.createDate) desc, time(im.createDate) asc").list();
        session.close();

        return imageList;
    }

    public Long getRowCount() {
        Session session = HibernateUtil.getSessionFactory().openSession();
        session.beginTransaction();
        /*Veritabanında kaç kayıt olduğunu getir.*/
        Long rowCount = (Long)session.createCriteria(Image.class).setProjection(Projections.rowCount()).uniqueResult();
        session.close();

        return rowCount;
    }

    public Image getBiggestImage() {
        Session session = HibernateUtil.getSessionFactory().openSession();
        session.beginTransaction();
        /*Veritabanındaki en büyük resmi getir.*/
        Query query = session.createQuery("from Image im order by length(im.image) desc");
        query.setMaxResults(1);
        Image image = (Image)query.uniqueResult();
        session.close();

        return image;
    }

    public Image save(Image image) {
        Session session = HibernateUtil.getSessionFactory().openSession();
        session.beginTransaction();
        session.save(image);
        session.getTransaction().commit();
        session.close();

        return image;
    }

    public void delete(Image image) {
        Session session = HibernateUtil.getSessionFactory().openSession();
        session.beginTransaction();
        session.delete(image);
        session.getTransaction().commit();
        session.close();
    }
}
