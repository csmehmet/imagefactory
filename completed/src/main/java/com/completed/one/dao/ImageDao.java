package com.completed.one.dao;

import com.completed.one.domain.Image;
import java.util.List;

public interface ImageDao {
    public Image getById(Integer imageId);
    public List<Image> getList();
    public Image save(Image image);
    public void delete(Image image);
    public Long getRowCount();
    public Image getBiggestImage();
}
