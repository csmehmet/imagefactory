package com.completed.one.resource;

import com.completed.one.dto.ImageDTO;
import com.completed.one.exception.GenericException;
import com.completed.one.service.ImageService;
import com.completed.one.utils.PropertyReader;
import org.apache.log4j.BasicConfigurator;
import org.apache.log4j.Level;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.util.List;

@Component
@Path("/Images")
public class ImageResource {
    private static final Logger logger = Logger.getLogger(ImageResource.class);

    @Autowired
    private ImageService imageService;

    @GET
    @Path("/")
    @Produces(MediaType.APPLICATION_JSON)
    public Response getImageList() {
        logger.info("getImageList begin");
        List<ImageDTO> imageDTOList = null;
        try {
            imageDTOList = imageService.getImageList();
        } catch (GenericException ex) {
            logger.warn(ex.getKey() + " " + ex.getMessage());
        } catch (Exception ex) {
            logger.error(ex, ex);
            return Response.status(Response.Status.INTERNAL_SERVER_ERROR).entity("error: " + ex.toString()).build();
        }
        logger.info("getImageList end");

        return Response.ok(imageDTOList).build();
    }

    @POST
    @Path("/")
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    public Response addImage(ImageDTO imageDTO) {
        logger.info("addImage begin");
        try {
            imageDTO = imageService.addImage(imageDTO);
        } catch (GenericException ex) {
            logger.warn(ex.getKey() + " " + ex.getMessage());
            return Response.status(Response.Status.INTERNAL_SERVER_ERROR).entity(ex.getMessage()).build();
        } catch (Exception ex) {
            logger.error(ex, ex);
            return Response.status(Response.Status.INTERNAL_SERVER_ERROR).entity("error: " + ex.toString()).build();
        }
        logger.info("addImage end");

        return Response.ok(imageDTO).build();
    }

    @DELETE
    @Path("/{imageId}")
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    public Response deleteImage(@PathParam("imageId") Integer imageId) {
        logger.info("deleteImage begin");
        ImageDTO imageDTO = null;
        try {
            imageDTO = imageService.deleteImage(imageId);
        } catch (Exception ex) {
            logger.error(ex, ex);
            return Response.status(Response.Status.INTERNAL_SERVER_ERROR).entity("error: " + ex.toString()).build();
        }
        logger.info("deleteImage end");

        return Response.ok(imageDTO).build();
    }
}
