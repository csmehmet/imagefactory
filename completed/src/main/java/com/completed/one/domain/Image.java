package com.completed.one.domain;

import com.completed.one.dto.ImageDTO;
import javax.persistence.*;
import java.io.Serializable;

@Entity
@Table(name = "IMAGES")
public class Image implements Serializable {

    @Id
    //@GeneratedValue(strategy = GenerationType.AUTO, generator = "id_Sequence")
    //@SequenceGenerator(name = "id_Sequence", sequenceName = "SEQ_CARS", allocationSize = 1)
    @GeneratedValue(strategy=GenerationType.AUTO)
    @Column(name = "ID")
    private Integer imageId;

    @Column(name = "IMAGE")
    private String image;

    @Column(name = "IMAGE_DESC")
    private String imageDesc;

    @Column(name = "CREATE_DATE")
    private String createDate;

    public Image() {

    }

    public Image(ImageDTO imageDTO) {
        this.setImageId(imageDTO.getImageId());
        this.setImage(imageDTO.getImage());
        this.setImageDesc(imageDTO.getImageDesc());
        this.setCreateDate(imageDTO.getCreateDate());
    }

    public ImageDTO toDTO() {
        ImageDTO imageDTO = new ImageDTO();
        imageDTO.setImageId(this.getImageId());
        imageDTO.setImage(this.getImage());
        imageDTO.setImageDesc(this.getImageDesc());
        imageDTO.setCreateDate(this.getCreateDate());

        return imageDTO;
    }

    public Integer getImageId() {
        return imageId;
    }

    public void setImageId(Integer imageId) {
        this.imageId = imageId;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public String getImageDesc() {
        return imageDesc;
    }

    public void setImageDesc(String imageDesc) {
        this.imageDesc = imageDesc;
    }

    public String getCreateDate() {
        return createDate;
    }

    public void setCreateDate(String createDate) {
        this.createDate = createDate;
    }
}
