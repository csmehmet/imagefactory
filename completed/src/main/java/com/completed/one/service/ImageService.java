package com.completed.one.service;

import com.completed.one.dao.ImageDao;
import com.completed.one.domain.Image;
import com.completed.one.dto.ImageDTO;
import com.completed.one.exception.GenericException;
import com.completed.one.utils.PropertyReader;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@Service
public class ImageService {

    @Autowired
    ImageDao imageDao;

    @Autowired
    private PropertyReader propertyReader;

    @Transactional(readOnly = true)
    public List<ImageDTO> getImageList() throws GenericException {
        List<ImageDTO> imageDTOList = new ArrayList<ImageDTO>();
        List<Image> imageList = imageDao.getList();
        if(imageList == null || imageList.size() == 0) {
            throw new GenericException("Kayıt yok","Veritabanında kayıt yok",GenericException.WARNING);
        }
        for (Image image : imageList) {
            imageDTOList.add(image.toDTO());
        }

        return imageDTOList;
    }

    @Transactional
    public ImageDTO addImage(ImageDTO imageDTO) throws GenericException {
        /*Dosya seçilmezse fron end kısmına hata döndür*/
        if(imageDTO == null || imageDTO.getImage() == null) {
            throw new GenericException("Pick a Image","You must pick a image",GenericException.ERROR);
        }
        /* Dosya property'dne okunan maksimum byte'den büyükse front end kısma hata döndür. */
        Long maxImageSize = Long.valueOf(propertyReader.getProperty("maxImageSize"));
        if(imageDTO.getImage().length() > maxImageSize) {
            throw new GenericException("File Size Problem","Image must be smaller than 5 mb",GenericException.ERROR);
        }
        Long rowCount = imageDao.getRowCount();
        /*Property dosyasından yuklenebilecek maksimum resim sayısını belirleriz*/
        Long maxImageCount = Long.valueOf(propertyReader.getProperty("maxImageCount"));
        if(rowCount >= maxImageCount) {
            Image image = imageDao.getBiggestImage();
            imageDao.delete(image);
        }
        DateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");
        Date date = new Date();
        imageDTO.setCreateDate(dateFormat.format(date));
        imageDao.save(new Image(imageDTO));

        return imageDTO;
    }

    @Transactional
    public ImageDTO deleteImage(Integer imageId) {
        Image image = imageDao.getById(imageId);
        imageDao.delete(image);

        return image.toDTO();
    }
}
