package com.completed.one.utils;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;
import org.springframework.core.env.Environment;

/**
 * Created by mehmet.keskin on 6/7/2017.
 */

@Configuration
@PropertySource("classpath:configuration/application.properties")
public class PropertyReader {

    @Autowired
    private Environment env;

    public String getProperty(String propertyName) {
       return env.getProperty(propertyName);
    }
}
