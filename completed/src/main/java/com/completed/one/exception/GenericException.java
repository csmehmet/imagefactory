package com.completed.one.exception;

/**
 * Created by mehmet.keskin on 6/7/2017.
 */
public class GenericException extends Exception {
    private static final long serialVersionUID = -6178650716150059325L;
    static public final int ERROR = 1,
            WARNING = 2,
            INFO = 3;

    private String message;
    private String key;

    public GenericException(String key, String message, int type) {
        this.key = key;
        this.message = message;
    }

    public String getMessage() {
        return this.message;
    }

    public String getKey() {
        return this.key;
    }
}
