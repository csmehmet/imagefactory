define(['require', 'jquery.min',
        'backbone-min',
        'utils/constants',
        'jquery.cookie-1.4.1.min',
        'underscore-1.8.3.min'
    ],
    function (require, $, Backbone, constants) {
        "use strict";

        return Backbone.Router.extend({
            currentView: null,
            routes: {
                '': 'index',
                'pages/image': 'image'
            },
            image: function () {
                var that = this;
                require(['app/views/imageview'], function (ImageView) {
                    if (that.currentView !== null) {
                        that.currentView.close();
                    }
                    var imageView = new ImageView();
                    that.currentView = imageView;
                });
            }
        });
    });