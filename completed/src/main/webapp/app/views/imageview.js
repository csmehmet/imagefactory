define(['jquery.min',
    'underscore-1.8.3.min',
    'backbone-min',
    'i18next-1.11.1.min',
    'handlebars-4.0.4.min',
    'text!component/tablesorter.tpl',
    'utils/constants',
    'app/models/Image',
    'text!template/image.tpl',
    'text!template/panels/imagesavepopup.tpl',
    'common/feedback',
    'moment',
    'app/libs/alertify.min.js',
    'jquery.tablesorter.min'
], function ($, _, Backbone, i18n, Handlebars, tableSorter, constants, ImageModel, ImagePage, ImageSaveModalPage, feedback, moment, alertify) {
    "use strict";
    return Backbone.View.extend({
        el: $('#appcontainer'),
        events: {
            'click .deleteBtn' : 'deleteEvent',
            'click .saveBtn' : 'saveModalEvent',
            'click #addBtn' : 'saveModalCompleteEvent',
            'click #imgInp' : 'uploadImage'
        },
        initialize: function () {
            this.fetch();
        },
        fetch: function () {
            var that = this;

            this.images = new ImageModel.Images();
            this.images.fetch({
                cache: false,
                processData: true,
                success: function (data) {
                    that.images = data.toJSON();
                    that.render();
                }, error: function (data) {
                    that.images = data.toJSON();
                    that.render();
                }
            });
        },
        render: function () {
            var that = this;
            var template = Handlebars.compile(ImagePage);
            var html = template({images: that.images});

            $('#appcontainer').html(html);
            $("html").i18n();
        },
        renderSaveModal: function() {
            var that = this;
            var template = Handlebars.compile(ImageSaveModalPage);
            var html = template();

            $('#imageSaveModalContainer').html(html);
            $("html").i18n();
        },
        deleteEvent: function (e) {
            var that = this;
            var id = $(e.currentTarget).attr('id').replace("delete", "");

            alertify.confirm("Silmek istediğinizden emin misiniz?", function (e) {
                if (e) {
                    that.images = new ImageModel.Image({id : id});
                    that.images.destroy({
                        success:function(){
                            that.fetch();
                        },
                        error:function(err){
                        }
                    });
                } else {
                }
            });
        },
        saveModalEvent: function(e) {
            this.renderSaveModal();
            $('#saveModal').modal('show');
        },
        saveModalCompleteEvent: function() {
            var that = this;
            var image = $('#img-upload').attr('src');
            var imageDesc = $('#imageDesc').val();

            that.images = new ImageModel.Image({image : image, imageDesc : imageDesc});
            that.images.save([],{
                success: function() {
                    that.fetch();
                    $('#saveModal').modal('hide');
                    $('.modal-backdrop').remove();
                    alertify.success("Ekleme basarili");
                },
                error: function(model, response) {
                    alertify.error(response.responseText);
                }
            });
        },
        uploadImage : function() {
            $('#img-upload').attr('src', undefined);

            $(document).on('change', '.btn-file :file', function() {
                var input = $(this),
                    label = input.val().replace(/\\/g, '/').replace(/.*\//, '');
                input.trigger('fileselect', [label]);
            });

            $('.btn-file :file').on('fileselect', function(event, label) {

                var input = $(this).parents('.input-group').find(':text'),
                    log = label;

                if( input.length ) {
                    input.val(log);
                } else {
                    if( log ) alert(log);
                }

            });
            function readURL(input) {
                if (input.files && input.files[0]) {
                    var reader = new FileReader();

                    reader.onload = function (e) {
                        $('#img-upload').attr('src', e.target.result);
                    }

                    reader.readAsDataURL(input.files[0]);
                }
            }

            $("#imgInp").change(function(){
                readURL(this);
            });
        }

    });
});