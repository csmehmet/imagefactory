<div class="modal fade" id="saveModal" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">New Image</h4>
            </div>
            <div class="modal-body">
                <div class="row">
                    <div class="container">
                    <div class="col-md-6">
                        <div class="form-group">
                            <label>Upload Image</label>
                            <div class="input-group">
                                <span class="input-group-btn">
                                    <span class="btn btn-default btn-file">
                                        Browse… <input type="file" id="imgInp">
                                    </span>
                                </span>
                                <input type="text" class="form-control" readonly>
                            </div>
                        </div>
                    </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-9">
                        <div class="form-group">
                            <input type="text" class="form-control" id="imageDesc" placeholder="Image Desc">
                        </div>
                    </div>
                    <div class="col-md-3">
                        <button type="button" id="addBtn" data-loading-text="Ekleniyor" class="btn btn-primary pull-right" autocomplete="off">
                            Add
                        </button>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-12">
                        <img id='img-upload' class="img-responsive" alt="Cinque Terre"/>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>