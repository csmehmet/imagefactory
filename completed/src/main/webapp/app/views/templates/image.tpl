<div class="col-md-12 col-xs-12">
    <a class="saveBtn btn pull-right" data-target=".bs-example-modal-sm">
        <span style="font-size:2em;" class="glyphicon glyphicon-plus-sign"></span>
    </a>
</div>
<div class="row">
    <div class="col-md-12">
        <table id="imageTable" class="table table-bordered table-hover">
            <thead>
            <th>
                <span data-i18n="body.image"></span>
            </th>
            <th>
                <span data-i18n="body.desc"></span>
            </th>
            <th>
                <span data-i18n="body.createdate"></span>
            </th>
            <th>
                <span data-i18n="body.operations"></span>
            </th>
            </thead>

            {{#each images}}
            <tr>
                <td class="col-md-4">
                    <div class="col-md-12">
                        <img class="img-responsive" height="200" alt="Cinque Terre" src="{{image}}"/>
                    </div>
                </td>
                <td class="col-md-3">
                    {{imageDesc}}
                </td>
                <td class="col-md-3">
                    {{createDate}}
                </td>
                <td class="col-md-2">
                    <div class="col-md-6 col-xs-12">
                        <a class="deleteBtn" id="delete{{imageId}}">
                            <span style="font-size:2em;" class="glyphicon glyphicon-minus-sign">
                            </span>
                        </a>
                    </div>
                </td>
            </tr>
            {{/each}}
        </table>
    </div>
</div>

<div id="imageSaveModalContainer"></div>
<div id="imageUpdateModalContainer"></div>