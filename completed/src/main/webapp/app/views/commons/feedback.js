define(function (require) {
    var $ = require('jquery.min'),
        _ = require('underscore-1.8.3.min'),
        Backbone = require('backbone-min'),
        i18n = require('i18next-1.11.1.min'),
        Handlebars = require('handlebars-4.0.4.min'),
        tpl = require('text!template/feedback.tpl');

    var FeedbackView = Backbone.View.extend({
        tagName: 'div',
        className: 'alert-wrap',
        container: null,
        type: 'error',
        messages: [], //message:{type, key, message, text, lineNo}

        clear: function () {

        },

        initialize: function () {
        },

        events: {
            'click .close-button': 'closeFeedback'
        },

        render: function () {
            $('.feedback-container').html(this.$el);

            var that = this;
            var _tmp = Handlebars.compile(tpl);
            var rendered = _tmp({type: this.type, messages: this.messages});
            this.$el.html(rendered);

            // Element remove edildikten sonra event içindeki eventlerin
            // tekrar delegate edilmesi lazım
            $('.close-button').click(function () {
                that.closeFeedback();
            });
        },

        applyMessage: function (message) {
            message.text = "";

            if (message.message != null && message.message != "") {
                message.text += ", " + message.message;
            }

            if (message.type != null) {
                this.type = message.type.toLowerCase();
            } else {
                this.type = "error";
            }

            this.messages.push(message);
        },

        applyMessages: function (msgs) {
            var i = 0;

            for (; i < msgs.length; i++) {
                this.applyMessage(msgs[i]);
            }
        },

        applyFeedback: function (msgs) {
            this.$el.empty();
            this.closeFeedback();
            this.messages = [];

            if ($.isArray(msgs)) {
                this.applyMessages(msgs);
            } else if (typeof msgs === "object" && !$.isArray(msgs)) {
                this.applyMessage(msgs);
            }

            this.render();
        },

        closeFeedback: function () {
            this.unbind();
            this.remove();
        }
    });

    return {
        Feedback: FeedbackView
    }
});