define(['jquery.min',
    'underscore-1.8.3.min',
    'backbone-min',
    'i18next-1.11.1.min',
    'handlebars-4.0.4.min',
    'text!template/header.tpl',
    'jquery.cookie-1.4.1.min',
    'bootstrap.min'
], function ($, _, Backbone, i18n, Handlebars, tpl) {
    "use strict";

    return Backbone.View.extend({
        el: $('#header'),
        initialize: function () {
            this.render();
        },
        render: function () {
            $('#header').html(Handlebars.compile(tpl));
        },
        events: {
            'click #langTR': 'tr',
            'click #langFR': 'fr',
            'click #langEN': 'en',
            'click #logout': 'logout'
        },
        tr: function () {
            i18n.setLng('tr', {fixLng: true}, function (tr) {
                if (window.location.hash == "#") {
                    $(document).trigger("change:language");
                }
            });
            $("html").i18n();
        },
        en: function () {
            i18n.setLng('en', {fixLng: true}, function (en) {
                if (window.location.hash == "#") {
                    $(document).trigger("change:language");
                }
            });
            $("html").i18n();
        },
        fr: function () {
            i18n.setLng('fr', {fixLng: true}, function (fr) {
                if (window.location.hash == "#") {
                    $(document).trigger("change:language");
                }
            });
            $("html").i18n();
        },
        logout: function () {
            $.removeCookie("username");
            $.removeCookie("role");
            $(window).scrollTop(0);
            window.location = "#login";
        }
    });
});