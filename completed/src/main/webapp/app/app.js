// RequireJS Configuration

requirejs.config({
    baseUrl: "app/libs",
    paths: {
        'app': '..',
        'template': '../views/templates',
        'component': '../views/component',
        'common': '../views/commons',
        'panel': '../views/templates/panels'
    },
    shim: {
        'jquery.min': {
            exports: '$'
        },
        'underscore-1.8.3.min': {
            exports: '_'
        },
        'backbone-min': {
            deps: ['jquery.min', 'underscore-1.8.3.min'],
            exports: 'Backbone'
        },
        'i18next-1.11.1.min': {
            deps: ['jquery.min'],
            exports: 'i18n'
        },
        "jquery-ui-1.11.4.min": {
            deps: ['jquery.min'],
            exports: "$"
        },
        "jquery.tablesorter.min": {
            deps: ['jquery.min'],
            exports: "$"
        },
        'handlebars-4.0.4.min': {
            exports: 'Handlebars'
        },
        'jquery.cookie-1.4.1.min': {
            deps: ['jquery.min'],
            exports: '$'
        },
        'jquery.validate-1.14.0.min': {
            deps: ['jquery.min']
        },
        'jquery.tablesorter.scroller': {
            deps: ['jquery.min', 'jquery.tablesorter.min']
        },
        'jquery.ui.widget': {
            deps: ['jquery.min']
        },
        'jquery.iframe-transport': {
            deps: ['jquery.min']
        },
        'jquery.fileupload': {
            deps: ['jquery.min', 'jquery-ui-1.11.4.min']
        },
        "moment": {
            exports: 'moment'
        },
        "jquery.fileDownload": {
            deps: ['jquery.min', 'jquery-ui-1.11.4.min']
        },
        "bootstrap.min": {
            deps: ['jquery.min']
        },
        "jquery-ui.min": {
            deps: ['jquery.min']
        },
        "alertify.min": {
            deps: ['jquery.min'],
            exports: 'alertify'
        }
    }
});

require(['jquery.min',
        'underscore-1.8.3.min',
        'backbone-min',
        'i18next-1.11.1.min',
        'handlebars-4.0.4.min',
        'app/views/commons/header-view',
        'app/router',
        'moment',
        'utils/constants',
        'jquery-ui-1.11.4.min',
        'jquery.tablesorter.min',
        'utils/menu',
        'jquery.min',
        'jquery.validate-1.14.0.min',
        'jquery.tablesorter.scroller',
        'jquery.ui.widget',
        'jquery.iframe-transport',
        'jquery.fileupload',
        'jquery.fileDownload',
        'jquery.cookie-1.4.1.min'],
    function ($, _, Backbone, i18n, Handlebars, headerView, Router, moment, constants) {

        $(document).on({
            ajaxStart: function () {
                $('#preloader').show();
            },
            ajaxStop: function () {
                $('#preloader').hide();
            },
            ajaxComplete: function () {
                $('#preloader').hide();
            },
            ajaxError: function () {
                $('#preloader').hide();
            },
            ajaxSuccess: function () {
                $('#preloader').hide();
            }
        });

        $(document).ready(function () {
            apiURL = 'http://localhost:8080/project/';
            defaultLang = 'en';
            initSettings(defaultLang);

        });


        //var customSync = function (method, model, options) {
        //    options.error = function (xhr, status) {
        //        if (xhr.status == 500 || xhr.status == 0) {
        //            alert("Server Error!");
        //        }
        //        if (xhr.status == 401) {
        //            $.removeCookie("username");
        //            $.removeCookie("role");
        //            window.location = "#login";
        //        }
        //    };
        //
        //    Backbone.$.ajaxSetup({
        //        crossDomain: true,
        //        xhrFields: {
        //            withCredentials: true
        //        }
        //    });
        //
        //    Backbone.sync(method, model, options);
        //};
        //
        //Backbone.Model.prototype.sync = customSync;
        //Backbone.Collection.prototype.sync = customSync;

        Backbone.View.prototype.close = function () {
            this.undelegateEvents();
        };

        function initSettings(defaultLang) {
            // i18next configuration
            var options = {
                resGetPath: "app/assets/translations/__lng__.json",
                preload: ['en', 'tr', 'fr'],
                fallbackLng: false,
                lng: defaultLang
            };

            // i18next init
            i18n.init(options, function (t) {
                $("html").i18n();
            });
        }

        var header = new headerView();
        var router = new Router();

        window.router = router;
        Backbone.history.start();

    });